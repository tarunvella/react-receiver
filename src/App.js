import React from 'react';
import {Chirp, CHIRP_SDK_STATE} from 'chirpsdk'
import logo from './loggedin1.jpg'
import "./App.css"

function Listner (props) {
  return (
    <button className = "listen-btn" onClick={props.onClick}>Listen</button>
  );
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.sdk = null;
    this.username= '';
    this.handleListner = this.handleListner.bind(this)
    this.state = {
      started: false,
      received: 'click start listening',
      disabled: false,
      initiate: false,
    }
  }

  toAscii(payload) {
    let str = ''
    for (let i = 0; i < payload.length; i++) {
      str += String.fromCharCode(payload[i])
    }
    return str
  }

   decodePayload(payload) {
     this.username = payload.split(':',1)

   }
  
  
  async startSDK() {
    this.sdk = await Chirp({
      key: '7DB75cfdD039fd41cf1b2aACE',
      onReceiving: () => {
        this.setState({
          received: 'start Receiving',
          disabled: true
        })
      },
      onReceived: data => {
        if (data.length > 0) {
          this.decodePayload(this.toAscii(data))
          this.setState({
            received: this.toAscii(data),
            disabled: false,
            initiate: true
          })
        } else {
          this.setState({
            received: `I didn't hear that. Try turning the volume up?`,
            disabled: false,
            initiate: false
          })
        }
      }
    })
    this.setState({ started: true })
  }

  handleListner(e) {
    this.setState({received: true})
    this.sdk.start().then(() => {
      if (this.sdk.getState() === CHIRP_SDK_STATE.RUNNING) {
        console.log('data in handleListner')
        this.sdk.setListenToSelf(e)
      }
    });
  }

  stopSdk() {
    this.sdk.stop();
    this.setState({
      started: false,
      received: 'click start listening',
      disabled: false,
      initiate: false,
    })
  }

  render() {
    const isStarted = this.state.started;
    const isSuceess = !this.state.initiate;
    return (
      <div>
      {/* <img src={logo} alt="swoosh" width="100" height="100" className="logo"></img> */}
      {isSuceess ?
      (<div className="container">
        <h1 className="app-title">Chirp Receiver</h1>
        <br />
        {isStarted ? (<Listner onClick={this.handleListner} />
        ):(
        <button 
        className="add-btn"
        onClick={() => this.startSDK()}
        >{this.state.received}
        </button>
        )}
        <div className="received-message">
          {<p>{this.state.received}</p>}
        </div></div>
        ):(
      <div className="welcome">
        <img src={logo}/>
        <div className="top">Welcom {this.username}!</div>
        <div className="top-two">You are logged in successfully!</div>
        <button 
        className="add-btn"
        onClick={() => this.stopSdk()}
        >Stop
        </button></div>)}
      </div>
    )
  }
}
export default App
